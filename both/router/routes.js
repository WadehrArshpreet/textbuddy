Router.map(function() {
    this.route('home', {
        path: '/',
        template: 'home',
        onBeforeAction: function() {
            var currentUser = Meteor.userId();
            if (currentUser) Router.go('user');
            this.next();
        }
    });
    this.route('redirect', {
        path: '/url',
        template: 'home',
        onBeforeAction: function() {
            window.location.replace(this.params.query.u);
            this.next();
        },
        fastRender: true
    });
    this.route('user', {
        path: '/u',
        waitOn: function() {
            var currentUser = Meteor.userId();
            if (!currentUser) Router.go('home');
            return [Meteor.subscribe('userData'),Meteor.subscribe('friendList'),Meteor.subscribe('UserChat')];
        },
        onBeforeAction: function() {
            var currentUser = Meteor.userId();
            if (!currentUser) Router.go('home');
            this.next();
        }
    });
})