//SETUP Facebook OAuth Configuration
ServiceConfiguration.configurations.remove({
    service: "facebook"
});
ServiceConfiguration.configurations.insert({
    service: "facebook",
    appId: "1691441281166780",
    secret: "42eb93e88eff68ed23be0b5d88c58e7e",
    requestPermissions: ['user_friends']
});
Accounts.onCreateUser(function(options, user) {
    if (user.services) {
        var service = _.keys(user.services)[0];
        var email = user.services[service].email;
        if (service == 'facebook') {
            if (!user.profile) user.profile = {};
            if (!user.profile.name) user.profile.name = user.services[service].name;
        }
        // else if (service == 'twitter') {
        //     if (!user.profile) user.profile = {};
        //     if (!user.profile.name) user.profile.name = user.services[service].screenName;
        // }
        if (!email) return user;
        //later will support for twitter
        var existingUser = Meteor.users.findOne({
            "services.twitter.email": email
        }) || Meteor.users.findOne({
            "services.facebook.email": email
        });
        user.profile.createdAt = new Date();
        user.profile.status = "Available";
        Friends.insert({
            userId: user._id,
            friends: [],
            pendingFriends: [],
            blockFriends: [],
            pendingRequests:[],
            lastUpdate: new Date(),
            createdAt: new Date()
        });
        if (!existingUser) return user;
        existingUser.services[service] = user.services[service];
        Meteor.users.remove({
            _id: existingUser._id
        }); // remove existing record
        return existingUser; // record is re-inserted
    }
});
Accounts.validateLoginAttempt(function(attempt) {
    if (attempt.user) {
        if (attempt.user.services.facebook && attempt.user.username == undefined) {
            name = attempt.user.services.facebook.name.replace(/ /g, "_");
            if (Meteor.users.find({
                    username: name
                }).count() > 1) {
                name = name + Meteor.users.find({
                    username: name
                }).count().toString();
            }
            Meteor.users.update({
                _id: attempt.user._id
            }, {
                $set: {
                    username: name
                }
            });
        }
    }
    return true;
});