Meteor.publish("userData", function() {
    return Meteor.users.find({
        _id: this.userId
    }, {
        fields: {
            'status': 1,
            'profile.status': 1,
            'services.twitter': 1,
            'services.facebook': 1
        }
    });
});
Meteor.publish('UserChat', function() {
    return UserChat.find({
        users: {
            $in: [this.userId]
        }
    });
});
Meteor.smartPublish("friendList", function() {
    var data = Friends.find({
        userId: this.userId
    }, {
        fields: {
            'friends': 1,
            'pendingFriends': 1,
            'pendingRequest': 1,
            'blockFriends': 1
        }
    });
    var retData = [data];
    var fetchData = data.fetch()[0];
    if (fetchData.friends != undefined)
        for (i = 0; i < fetchData.friends.length; i++) {
            var tData = Meteor.users.find({
                _id: fetchData.friends[i]
            }, {
                fields: {
                    'status': 1,
                    'profile.status': 1,
                    'services.facebook.id': 1,
                    'services.facebook.name': 1
                }
            });
            retData.push(tData);
        }
    if (fetchData.pendingRequest != undefined)
        for (i = 0; i < fetchData.pendingRequest.length; i++) {
            var tData = Meteor.users.find({
                _id: fetchData.pendingRequest[i]
            }, {
                fields: {
                    'status': 1,
                    'profile.status': 1,
                    'services.facebook.id': 1,
                    'services.facebook.name': 1
                }
            });
            retData.push(tData);
        }
    return retData;
});