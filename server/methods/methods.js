Meteor.methods({
    'addFriend': function(email) {
        var friends = Friends.findOne({
            userId: Meteor.userId()
        });
        //check if user exist
        var user = Meteor.users.findOne({
            "services.facebook.email": email
        });
        if (user == undefined) throw new Meteor.Error(400, "User Not Exist");
        //check if already friend
        if (friends.friends.indexOf(user._id) != -1) throw new Meteor.Error(401, "Already Friend");
        if (friends.pendingFriends != undefined && friends.pendingFriends.indexOf(user._id) != -1) throw new Meteor.Error(402, "Already Chat Request Sent");
        if (friends.pendingRequest != undefined && friends.pendingRequest.indexOf(user._id) != -1) {
            Meteor.call('acceptFriend', user._id);
            return true;
        }
        //check other user block list
        var friendsFriend = Friends.findOne({
            userId: user._id
        });
        if (friendsFriend.blockFriends.indexOf(user._id) != -1) throw new Meteor.Error(403, "Cannot Add this User");
        //add pending friend can start one way communication
        var pendingFriends = friends.pendingFriends;
        if (pendingFriends == undefined) pendingFriends = [];
        pendingFriends.push(user._id);
        Friends.update({
            userId: Meteor.userId()
        }, {
            $set: {
                pendingFriends: pendingFriends,
                lastUpdate: new Date()
            }
        });
        //add pending request to user
        var pendingRequest = friendsFriend.pendingRequest;
        if (pendingRequest == undefined) pendingRequest = [];
        pendingRequest.push(Meteor.userId());
        Friends.update({
            userId: user._id
        }, {
            $set: {
                pendingRequest: pendingRequest,
                lastUpdate: new Date()
            }
        });
        return true;
    },
    'acceptFriend': function(id) {
        var friends = Friends.findOne({
            userId: Meteor.userId()
        });
        var friendsFriend = Friends.findOne({
            userId: id
        });
        //remove from pendingRequest for user 1
        var pendingRequest = friends.pendingRequest;
        var index = pendingRequest.indexOf(id);
        if (index == -1) throw new Meteor.Error(402, "No Request Found for accepting");
        pendingRequest.splice(index, 1);
        //remove from pendingFriends for other user
        var pendingFriends = friendsFriend.pendingFriends;
        var index = pendingFriends.indexOf(Meteor.userId());
        if (index == -1) throw new Meteor.Error(402, "No Friend Found");
        pendingFriends.splice(index, 1);
        //add to friends for both user
        if (friends.friends.indexOf(id) != -1 && friendsFriend.friends.indexOf(Meteor.userId())) return true;
        var tempFriend = friends.friends;
        tempFriend.push(id);
        var tempOtherFriend = friendsFriend.friends;
        tempOtherFriend.push(Meteor.userId());
        Friends.update({
            userId: Meteor.userId()
        }, {
            $set: {
                lastUpdate: new Date(),
                friends: tempFriend,
                pendingRequest: pendingRequest
            }
        });
        Friends.update({
            userId: id
        }, {
            $set: {
                lastUpdate: new Date(),
                friends: tempOtherFriend,
                pendingFriends: pendingFriends
            }
        });
        //create chat b/w friends
        UserChat.insert({
            createdAt: new Date(),
            lastUpdate: new Date(),
            single: true,
            users: [Meteor.userId(), id],
            message: [{
                message: "You are now friend. Say Hi to Start Chat.",
                isCenter: true
            }]
        });
        //{
        //   message: ,
        //   timestamp: ,
        //   user: ,
        //   isRead:
        //}
        return true;
    },
    'blockFriend': function(id) {
        var friends = Friends.findOne({
            userId: Meteor.userId()
        });
        var friendsFriend = Friends.findOne({
            userId: id
        });
        //remove from pendingRequest for user 1
        var pendingRequest = friends.pendingRequest;
        var index = pendingRequest.indexOf(id);
        if (index == -1) throw new Meteor.Error(402, "No Request Found for accepting");
        pendingRequest.splice(index, 1);
        //remove from pendingFriends for other user
        var pendingFriends = friendsFriend.pendingFriends;
        var index = pendingFriends.indexOf(Meteor.userId());
        if (index == -1) throw new Meteor.Error(402, "No Friend Found");
        pendingFriends.splice(index, 1);
        var tempFriend = friends.blockFriends;
        tempFriend.push(id);
        Friends.update({
            userId: Meteor.userId()
        }, {
            $set: {
                lastUpdate: new Date(),
                blockFriends: tempFriend,
                pendingRequest: pendingRequest
            }
        });
        Friends.update({
            userId: id
        }, {
            $set: {
                lastUpdate: new Date(),
                pendingFriends: pendingFriends
            }
        });
        return true;
    },
    setOffline: function(id) {
        Meteor.users.update({
            _id: id
        }, {
            $set: {
                "profile.isOnline": false
            }
        });
    },
    sendChat: function(id, val) {
        var chat = UserChat.findOne({
            _id: id
        });
        if (chat.users.indexOf(Meteor.userId()) == -1) throw new Meteor.Error(402, "Error in sending Chat");
        var message = chat.message;
        var newObj = {
            message: val,
            timestamp: new Date(),
            user: Meteor.userId(),
            isRead: false
        }
        message.push(newObj);
        UserChat.update({
            _id: id
        }, {
            $set: {
                lastUpdate: new Date(),
                message: message
            }
        });
        return true;
    },
    allRead: function(id) {
        var chat = UserChat.findOne({
            _id: id
        }).message;
        for (i = 0; i < chat.length; i++) {
            if (chat[i].user != Meteor.userId() && chat[i].isRead == false) {
                chat[i].isRead = true;
            }
        }
        UserChat.update({
            _id: id
        }, {
            $set: {
                lastUpdate: new Date(),
                message: chat
            }
        });
    },
    statusUpdate: function(status) {
        Meteor.users.update({
            _id: Meteor.userId()
        }, {
            $set: {
                "profile.status": status
            }
        });
        return true;
    }
});