Template.home.onRendered(function() {})
Template.home.events({
    'click #signInFacebook': function(e, tmp) {
        Meteor.loginWithFacebook({
            requestPermissions: ['email', 'public_profile', 'user_friends']
        }, function(err) {
            if (err) {
                //error handling
                // alert('error : '+err.message);
            } else {
                Router.go('user');
            }
        });
    }
});