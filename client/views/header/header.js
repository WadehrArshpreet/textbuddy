Template.header.helpers({
    userData: function() {
        if (Meteor.user()) return Meteor.user().services.facebook;
    },
    imageURL: function(id) {
        if (Meteor.user() && (id == undefined || id == null)) return "https://graph.facebook.com/" + Meteor.user().services.facebook.id + "/picture?width=150";
        if (Meteor.users.findOne({
                _id: id
            })) return "https://graph.facebook.com/" + Meteor.users.findOne({
            _id: id
        }).services.facebook.id + "/picture?width=150";
    },
    getName(id) {
        if (Meteor.users.findOne({
                _id: id
            })) return Meteor.users.findOne({
            _id: id
        }).services.facebook.name;
    },
    requestCount() {
        if (Friends.findOne() && Friends.findOne().pendingRequest != undefined) return Friends.findOne().pendingRequest.length;
        return false;
    },
    getRequest() {
        if (Friends.findOne() && Friends.findOne().pendingRequest != undefined) return Friends.findOne().pendingRequest;
    },
    getFriends() {
        if (Friends.findOne() && Friends.findOne().friends != undefined) return Friends.findOne().friends;
    },
    online(id) {
        if (Meteor.user() && id == null) return Meteor.user().status.online;
        return Meteor.users.findOne({
            _id: id
        }).status.online;
    },
    status(id) {
        if (Meteor.user() && id == null) return Meteor.user().profile.status;
        return Meteor.users.findOne({
            _id: id
        }).profile.status;
    },
    isMySelf(){
        return Template.instance().data.instance.profileSelected.get() == Meteor.userId();
    },
    selectedProfileId() {
        return Template.instance().data.instance.profileSelected.get();
    },
    chatSelected() {
        if ($(window).width() < 992) return Template.instance().data.instance.selectChatId.get();
        return false;
    }
});
Template.header.events({
    'click #logout': function(e, tmp) {
        e.preventDefault();
        var id = Meteor.userId();
        Meteor.logout(function(err) {
            Meteor.call('setOffline', id);
        });
    },
    'submit #addFriend': function(e, tmp) {
        e.preventDefault();
        var email = e.target.emailId.value;
        if (email == Meteor.user().services.facebook.email) {
            toastr.warning("Cannot Add Yourself");
            setTimeout(function() {
                $('#addFriend').trigger('reset');
                $('#newFriend').modal('hide');
            }, 500);
            return false;
        }
        Meteor.call('addFriend', email, function(err, res) {
            if (err) {
                if (err.error == 400) {
                    toastr.warning(err.reason);
                    //ask to send invite
                    bootbox.confirm({
                        title: "Send Invite?",
                        message: "Do you want to Invite " + email + " to TextBuddy",
                        buttons: {
                            cancel: {
                                label: '<i class="fa fa-times"></i> No'
                            },
                            confirm: {
                                label: '<i class="fa fa-check"></i> Yes'
                            }
                        },
                        callback: function(result) {
                            if (result) {
                                //true
                                //send invite to email
                            }
                        }
                    });
                } else toastr.warning(err.reason);
            } else toastr.success('request send successfully');
            setTimeout(function() {
                $('#addFriend').trigger('reset');
                $('#newFriend').modal('hide');
            }, 500);
        });
    },
    'click .acceptRequest': function(e, tmp) {
        var id = $(e.target).attr('friendId');
        Meteor.call('acceptFriend', id, function(error, result) {
            if (error) {
                toastr.warning(error.reason);
            } else {
                toastr.success("Successfully added to your friend list.");
            }
        });
    },
    'click .blockRequest': function(e, tmp) {
        var id = $(e.target).attr('friendId');
        Meteor.call('blockFriend', id, function(error, result) {
            if (error) {
                toastr.warning(error.reason);
            } else {
                toastr.success("Successfully blocked.");
            }
        });
    },
    'click .selectFriend': function(e, tmp) {
        var id = $(e.target).closest('tr').attr('friendId');
    },
    'click #goBack': function(e, tmp) {
        $('.inputChat').val('');
        $('.chatMessages').addClass('hidden-xs').addClass('hidden-sm');
        $('.friends').removeClass('hidden-xs').removeClass('hidden-sm');
        Template.instance().data.instance.selectChatId.set(null);
        $('#goBack').addClass('hide');
    },
    'click .editStatus': function(e, tmp) {
        if ($('#userStatus').hasClass('hide')) $('#userStatus').removeClass('hide');
        else $('#userStatus').addClass('hide');
        $('.userStatus span').toggle('fast');
    },
    'submit #changeStatus': function(e, tmp) {
        e.preventDefault();
        var val = $('#userStatus').val();
        Meteor.call('statusUpdate', val, function(e, r) {
            if (r) {
                $('#userStatus').addClass('hide');
                $('.userStatus span').toggle('fast');
            }
        })
    },
    'click .targetMyProfile':function(e,tmp){
        var tmp = tmp.data.instance;
        tmp.profileSelected.set(Meteor.userId());
    }
});