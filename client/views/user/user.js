// var firebase = require("firebase");
// var config = {
//     apiKey: "AIzaSyANi1nZFLuZbY1wBUo2YvbHb0B6eifY7Gc",
//     authDomain: "mylibrary-1231.firebaseapp.com",
//     databaseURL: "https://mylibrary-1231.firebaseio.com",
//     storageBucket: "mylibrary-1231.appspot.com",
//     messagingSenderId: "199809889412"
// };
// firebase.initializeApp(config);
// console.log(firebase);
function gotoLast(time) {
    if (time == undefined) time = 1;
    setTimeout(function() {
        var trueDivHeight = $('.chat')[0].scrollHeight;
        var divHeight = $('.chat').height();
        var scrollLeft = trueDivHeight - divHeight;
        $('.chat').scrollTop(scrollLeft);
    }, time);
}

function linkify(inputText) {
    var replacedText, replacePattern1, replacePattern2, replacePattern3;
    var replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    var searches = inputText.match(replacePattern1); //return array
    var replacedText = inputText;
    if (searches != null)
        for (var i = 0; i < searches.length; i++) replacedText = inputText.replace(searches[i], '<a href="' + window.location.origin + 'url?u=' + searches[i] + '" target="_blank">' + (searches[i].length > 60 ? searches[i].split("").splice(0, 60).join("") + "..." : searches[i]) + '</a>')
    var replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    var searches = inputText.match(replacePattern2); //return array
    if (searches != null)
        for (var i = 0; i < searches.length; i++) {
            searches[i] = $.trim(searches[i]);
            replacedText = replacedText.replace(searches[i], '<a href="' + window.location.origin + 'url?u=http://' + $.trim(searches[i]) + '" target="_blank">http://' + (searches[i].length > 55 ? (searches[i].split("").splice(0, 55).join("") + "...") : searches[i]) + '</a>');
        }
    var replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    var replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');
    return replacedText;
}
import emojify from 'emojify';
Template.user.onCreated(function() {
    this.selectChatId = new ReactiveVar(null);
    this.profileSelected = new ReactiveVar(null);
    this.chatData = new ReactiveVar([]);
    var self = this;
    if (Router.current().params.hash != null) {
        var hash = Router.current().params.hash;
        if (UserChat.find({
                _id: hash
            }).count() > 0) self.selectChatId.set(hash);
    }
    this.autorun(function() {
        if (self.selectChatId.get() == null) return;
        var chats = UserChat.findOne({
            _id: self.selectChatId.get()
        });

    });
    self.autorun(function() {
        self.chatData.set(UserChat.find({}, {
            sort: {
                lastupdate: -1
            }
        }).fetch());
    })
    self.chatData.set(UserChat.find({}, {
        sort: {
            lastupdate: -1
        }
    }).fetch());
});
Template.user.onRendered(function() {
    setTimeout(function() {
        //intialize toastr
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "2000",
            "timeOut": "2000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        emojify.setConfig({
            emojify_tag_type: 'div', // Only run emojify.js on this element
            only_crawl_id: null, // Use to restrict where emojify.js applies
            img_dir: '/images/emoji', // Directory for emoji images
            ignored_tags: { // Ignore the following tags
                'SCRIPT': 1,
                'TEXTAREA': 1,
                'A': 1,
                'PRE': 1,
                'CODE': 1
            }
        });
        ga('set', 'userId', Meteor.user().services.facebook.email);
    }, 1000);
    setInterval(function() {
        Session.set("time", new Date())
    }, 60000);
});
Template.user.helpers({
    inputDisplay(){
        var tmp = Template.instance();
        if(tmp.selectChatId.get() == null) return "hide";
    },
    getCount() {
        var count = 0;
        var chat = this.message;
        chat.map(function(c, index, ar) {
            if (c.user != Meteor.userId() && c.isRead == false) {
                count++;
            }
        });
        return count;
    },
    getSelectedChatUser() {
        var tmp = Template.instance();
        if (UserChat.findOne({
                _id: tmp.selectChatId.get()
            })) return UserChat.findOne({
            _id: tmp.selectChatId.get()
        }).users;
    },
    chatSelected() {
        var tmp = Template.instance();
        if (tmp.selectChatId.get() == null) return false;
        return true;
    },
    imageById: function(id) {
        if (Meteor.users.findOne({
                _id: id
            })) return "https://graph.facebook.com/" + Meteor.users.findOne({
            _id: id
        }).services.facebook.id + "/picture?width=150";
    },
    getNameById: function(id) {
        if (Meteor.users.findOne({
                _id: id
            })) return Meteor.users.findOne({
            _id: id
        }).services.facebook.name;
    },
    imageURL: function(userList) {
        if (Meteor.user() && userList == undefined) return "https://graph.facebook.com/" + Meteor.user().services.facebook.id + "/picture?width=150";
        var index = userList.indexOf(Meteor.userId());
        if (index != -1) userList.splice(index, 1); //remove own Id
        var id = userList[0];
        if (Meteor.users.findOne({
                _id: id
            })) return "https://graph.facebook.com/" + Meteor.users.findOne({
            _id: id
        }).services.facebook.id + "/picture?width=150";
    },
    getStatus: function(userList) {
        var index = userList.indexOf(Meteor.userId());
        if (index != -1) userList.splice(index, 1); //remove own Id
        var id = userList[0];
        var userStatus = Meteor.users.findOne({
            _id: id
        }).status;
        // Meteor.users.find({_id:id}).observe({
        // changed: function (newDocument, oldDocument) {
        // console.log(newDocument);
        // console.log(oldDocument);
        //} // Use either changed() OR(!) changedAt()
        // });
        // var lastLogin = (userStatus.lastLogin != undefined ? userStatus.lastLogin.date : new Date());
        // var lL = new Date(lastLogin);
        var isOnline = userStatus.online;
        // Session.get('time');
        if (isOnline) return true;
        else return false;
        // else return "Last Seen: " + moment(lL).fromNow();
    },
    getName(userList) {
        var index = userList.indexOf(Meteor.userId());
        if (index != -1) userList.splice(index, 1); //remove own Id
        var id = userList[0];
        if (Meteor.users.findOne({
                _id: id
            })) return Meteor.users.findOne({
            _id: id
        }).services.facebook.name;
    },
    getChats() {
        return Template.instance().chatData.get();
    },
    getLastMessage(messages) {
        return linkify(emojify.replace(messages[messages.length - 1].message));
    },
    lastMessageTime(that) {
        var messages = that.message;
        var time = new Date();
        if (messages[messages.length - 1].timestamp == undefined) time = that.lastupdate;
        else time = messages[messages.length - 1].timestamp;
        return moment(time).fromNow();
    },
    getTimeAgo(time) {
        Session.get('time');
        var time = new Date(time);
        return moment(time).fromNow();
    },
    checkLeft(obj) {
        if (obj.user == Meteor.userId()) return false;
        return true;
    },
    SelectedChat() {
        var tmp = Template.instance();
        var id = tmp.selectChatId.get();
        if (UserChat.findOne({
                _id: id
            }) != undefined) return UserChat.findOne({
            _id: id
        }).message;
    },
    template() {
        return Template.instance();
    },
    modifyPost(post) {
        return linkify(emojify.replace(post));
    }
});
Template.user.events({
    'click .chatIndex': function(e, tmp) {
        tmp.selectChatId.set(this._id);
        var self = this;
        $('.friend-list li').removeClass('active');
        $('.friend-list li').each(function() {
            if ($(this).attr('chatId') == self._id) {
                $(this).addClass('active');
            }
        });
        $('.chatMessages').removeClass('hidden-xs').removeClass('hidden-sm');
        $('.friends').addClass('hidden-xs').addClass('hidden-sm');
        $('#goBack').removeClass('hide');
        $('.inputChat').focus();
        Meteor.call('allRead', this._id);
        gotoLast(400);
    },
    'submit #newChat': function(e, tmp) {
        e.preventDefault();
        var val = $('.inputChat').val();
        if ($.trim(val) == "") return false;
        Meteor.call('sendChat', tmp.selectChatId.get(), val, function(e, r) {
            if (e) {} else {
                $('.inputChat').val('');
                $('.inputChat').focus();
                gotoLast();
            }
        });
    },
    'click .targetProfile': function(e, tmp) {
        var id = this.user;
        tmp.profileSelected.set(id);
    }
});